"use strict"

const urlAjax = "https://www.notifii.ca/wp-admin/admin-ajax.php";

var wpCookie;
var wpUser;
var userTimer;

function BeginGettingUser() {
	if (!userTimer) {
		userTimer = setInterval(t => {
			console.log("interval Begin");
			if (!wpCookie) {
				console.log("interval getting cookie");
				GetWPCookie();
			} else if (wpCookie === "noWPcookie") {
				console.log("interval no cookie");
				window.clearTimeout(userTimer);
				userTimer = null;
			} else if (!wpUser) {
				console.log("interval User");
				GetWPUser();
			} else {
				console.log("interval clear");
				window.clearTimeout(userTimer);
				userTimer = null;
			}
		}, 500);
	}
}

function GetWPCookie() {
	chrome.cookies.getAll({ "url": "https://www.notifii.ca" }, cookies => {
		cookies.forEach(cookie => {
			if (cookie.name.indexOf("wordpress_logged_in") !== -1) {
				wpCookie = cookie;
			}
		})
		if (!wpCookie) {
			wpCookie = "noWPcookie";
			wpUser = "noWPcookie";
		}
	});
}

function GetWPUser() {
	document.cookie = wpCookie;
	var act = {
		action: 'is_user_logged_in'
	};
	jQuery.post(urlAjax, act, ((response, status, xhr) => {
		var resUser = JSON.parse(response);
		if (xhr.status === 200) {
			if (resUser.user !== "no") {
				wpUser = resUser;
			} else {
				//callback(undefined);
			}
		} else {
			//something whent wrong
		}
	}));
}
