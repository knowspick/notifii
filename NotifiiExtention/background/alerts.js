"use strict"

class Alert {
    constructor(pDesc, pFilter) {
        this.id = undefined;
        this.desc = pDesc;
        this.filter = pFilter;
        this.lastRun = 0;
    }
}

var alertsArray;

function SaveToStorage() {
    let alertsJason = JSON.stringify(alertsArray);
    let payload = {
        "command": "saveAlerts",
        "alerts": alertsJason
    }
    if (chrome.runtime) {
        chrome.runtime.sendMessage(payload);
    }
}

function addUpdateAlertArray(myAlert) {
    if (!alertsArray) {
        alertsArray = [];
    };

    if (!myAlert.id) {
        myAlert.id = alertsArray.length;
    }
    alertsArray[myAlert.id] = myAlert;
    SaveToStorage();
}

/*
    remove(alert) {
        this._alerts.splice(this._alerts.indexOf(lert));
    }

    getNextAlertToRun() {
        return 0;
    }

    SearchSite() {
        var surl = "https://www.kijiji.ca/b-alberta/dog/k0l9003?rb=true&sort=relevancyDesc&dc=true";
        $.ajax({
            url: surl, success: function (result) {
                $("#div1").html(result);
            }
        });
    }
}

*/
