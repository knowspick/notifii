var AlertsData;

chrome.runtime.onMessage.addListener(
	function (request, sender, sendResponse) {
		console.log("onMessageBegin");

		switch (request.command) {
			case "clearUser":
				if (wpCookie === "noWPcookie") {
					wpCookie = undefined;
					wpUser = undefined;
				}
				break;

			case "getuser":
				sendResponse(wpUser);
				BeginGettingUser();
				break;

			case "saveAlerts":
				chrome.storage.sync.set({ "Alerts": request.alerts });
				break;

			case "getAlerts":
				sendResponse(AlertsData);
				if (!AlertsData) {
					chrome.storage.sync.get("Alerts", (data) => {
						AlertsData = data;
					});
				}
				break;

		}
	}
);

chrome.runtime.onInstalled.addListener(function () {
	//console.log("onInstalled");
	//GetUserDetails();
});