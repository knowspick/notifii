"use strict"

const urlLogin = "https://www.notifii.ca/login";
const urlAccount = "https://www.notifii.ca/account";

$(function () {
	DisplayUserDetails();

	LoadAlerts();
});

//#region UserDetails 

function DisplayUserDetails() {

	if (chrome.runtime !== undefined) {
		//Extention process
		chrome.runtime.sendMessage({ "command": "clearUser" });

		var timer = setInterval(t => {
			chrome.runtime.sendMessage({ "command": "getuser" }, response => {
				if (!response) {
					console.log("stil loading");
				} else if (response === "noWPcookie") {
					console.log("noWPcookie");
					DisplayNoUser();
					window.clearTimeout(timer);
					timer = null;
				} else {
					DisplayUser(response);
					window.clearTimeout(timer);
					timer = null;
				}
			});
		}, 500);
	} else {
		DisplayUser({ "user": "TestDude", "subsciption": "Premuim" });
	}

}

function DisplayUser(user) {
	var userDetails = "<a id='accLink' href='" + urlAccount + "'>" + user.user + "</a>";
	userDetails += "<br>";
	userDetails += user.subsciption;
	$("#userField").html(userDetails);
	$("#accLink").click(() => {
		chrome.tabs.create({ url: urlAccount });
	});
}

function DisplayNoUser() {
	$("#userField").html("<a id='loginLink' href='" + urlLogin + "'>Please login</a>");
	$("#loginLink").click(() => {
		chrome.tabs.create({ url: urlLogin });
	});
}

//#endregion Userdetails

